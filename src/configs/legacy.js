const config = {
  aws_project_region: 'us-east-1',
  aws_cognito_identity_pool_id:
    'us-east-1:2f9a772b-6b4a-42d5-b0c7-a51bf72859ae',
  aws_cognito_region: 'us-east-1',
  aws_user_pools_id: 'us-east-1_MULM4hRYg',
  aws_user_pools_web_client_id: '6gtkeamcas2ffkr7secork4ucq',
  oauth: {},
  aws_appsync_graphqlEndpoint:
    'https://xvkytsjpf5aljdmcyqazvnqch4.appsync-api.us-east-1.amazonaws.com/graphql',
  aws_appsync_region: 'us-east-1',
  aws_appsync_authenticationType: 'AMAZON_COGNITO_USER_POOLS',

  aws_user_files_s3_bucket: 'xgt-mark-filesmvp-dev',
  aws_user_files_s3_bucket_region: 'us-east-1',
};

module.exports = config;
