const AppSyncService = require('../core/appsync-service');
const interactions = require('../interactions/system-pricing-tag');


/**
 * 
 * SystemPricingTag Service provides an interface 
 * for SystemPricingTag API
 * 
 */
class SystemPricingTagService extends AppSyncService {

  getConfiguration() {
    return  require('../configs/system-pricing-tag');
  }
  
  /**
   * List SystemPricingTags
   */
  async list() {
    return await this.invoke(interactions.list);
  };

}

module.exports = SystemPricingTagService;