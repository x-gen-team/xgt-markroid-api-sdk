module.exports.legalEntity = {
  list: /* GraphQL */ `
    query ListLegalEntities {
      legalEntities: listLegalEntities {
        id
        name
        description
      }
    }
  `,
  get: /* GraphQL */ `
    query GetLegalEntity($id: Int!) {
      legalEntity: getLegalEntity(id: $id) {
        id
        name
        description
      }
    }
  `
};

module.exports.industry = {
  list: /* GraphQL */ `
    query ListIndustries {
      industries: listIndustries {
        id
        name
        markets {
          id
          name
        }
      }
    }
  `,
  get: /* GraphQL */ `
    query GetIndustry($id: Int!) {
      industry: getIndustry(id: $id) {
        id
        name
        markets {
          id
          name
        }
      }
    }
  `
};

