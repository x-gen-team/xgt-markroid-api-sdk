const interactions          = require('../../interactions/offering');
const AppSyncService = require('../../core/appsync-service');

/**
 * 
 * Offering Service provides an interface 
 * for Offering API
 * 
 */

class OfferingService extends AppSyncService{

  getConfiguration() {
    return  require('../../configs/legacy');
  }

  /**
   * List Offerings
   */
  async list () {
    return await this.invoke(
      interactions.list
    );
  }

  /**
   * Get Offering By Id
   */
  async getById ({offeringId}) {
    return await this.invoke(
      interactions.getById,
      { offeringId }
    );
  }

  /**
   * Get Offering By Page Name
   */
  async getByPageName ({pageName}) {
    return await this.invoke(
      interactions.getByPageName,
      { pageName }
    );
  }

  /**
   * Create Offering
   */
  async create ({input}) {
    return await this.invoke(
      interactions.create,
      { input }
    );
  }

  async getTypes () {
    return {
      data: {
        types: [
          'Digital',
          'Physical',
          'Service'
        ]
      }
    };
  };
  
  async getCommercialTypes () {
    return {
      data: {
        commercialTypes: [
          'B2B',
          'B2C'
        ]
      }
    }
  };
 
  /**
   * Delete Offering
   */
  async delete ({offeringId}) {
    return await this.invoke(
      interactions.delete,
      { offeringId }
    );

  };

};

OfferingService.types = {
  'Digital': 'Digital',
  'Physical': 'Physical',
  'Service': 'Service'
};

OfferingService.commercialTypes = {
  'B2B': 'B2B',
  'B2C': 'B2C'
};

module.exports = OfferingService;