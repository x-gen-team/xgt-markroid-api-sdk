module.exports.get = /* GraphQL */ `
  query GetMetadata($metadataId: String!) {
    metadata: getMetadata(metadataId: $metadataId) {
      id
      data
    }
  }
`;