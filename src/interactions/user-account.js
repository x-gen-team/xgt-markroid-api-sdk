module.exports.getInfo = /* GraphQL */ `
  query GetInfo {
    accountInfo: getInfo {
      id
      billing {
        freeTrialDays
        estimatedBill
        budget
        spendings
      }
      registeredAt
    }
  }
`;

module.exports.create = /* GraphQL */ `
  mutation Create {
    accountInfo: create {
      id
      billing {
        freeTrialDays
        estimatedBill
        budget
        spendings
      }
      registeredAt
    }
  }
`;
