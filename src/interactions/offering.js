module.exports.getById = /* GraphQL */ `
  query GetOfferingById($offeringId: ID!) {
    offering: getOfferingById(offeringId: $offeringId) {
      id
      name
      logo
      description
      pageName
      type
      market {
        id
        name
      }
      strategy {
        id
        type
        name
        description
        mode
        sections
        details {
          state
          objectives {
            id
            name
            isEnabled
            criteria {
              id
              name
              importance
            }
            actions {
              id
              name
              description
              isAutomated
              state
              due
              checkpoints
            }
            resources
            integrations
          }
          notifications{
            type
            message
            actionItems
          }
          settings {
            trackingIntegrations {
              id
              type
              name
              icon
            }
            subscriptions {
              id
              type
              receiver
            }
            team {
              accountId
              name
              role{
                id
                name
                description
                permissions {
                  service
                  access
                }
              }
              ownership
              contracts{
                id
                name
                description
                url
              }
            }
          }
        }
      }
      isCommercial
      commercialType
      valueProposition {
        features {
          id
          type
          description
        }
        benefits {
          id
          type
          description
        }
        competitiveAdvantages {
          id
          type
          description
        }
      }
    }
  }
`;

module.exports.getByPageName = /* GraphQL */ `
  query GetOfferingByPageName($pageName: String!) {
    offering: getOfferingByPageName(pageName: $pageName) {
      id
      name
      logo
      description
      pageName
      type
      market {
        id
        name
      }
      strategy {
        id
        type
        name
        description
        mode
        sections
        details {
          state
          objectives {
            id
            name
            isEnabled
            criteria {
              id
              name
              importance
            }
            actions {
              id
              name
              description
              isAutomated
              state
              due
              checkpoints
            }
            resources
            integrations
          }
          notifications{
            type
            message
            actionItems
          }
          settings {
            trackingIntegrations {
              id
              type
              name
              icon
            }
            subscriptions {
              id
              type
              receiver
            }
            team {
              accountId
              name
              role{
                id
                name
                description
                permissions {
                  service
                  access
                }
              }
              ownership
              contracts{
                id
                name
                description
                url
              }
            }
          }
        }
      }
      isCommercial
      commercialType
      valueProposition {
        features {
          id
          type
          description
        }
        benefits {
          id
          type
          description
        }
        competitiveAdvantages {
          id
          type
          description
        }
      }
    }
  }
`;

module.exports.list = /* GraphQL */ `
  query ListOfferings {
    offerings: listOfferings {
      id
      name
      logo
      description
      pageName
      type
    }
  }
`;

module.exports.create = /* GraphQL */ `
  mutation CreateOffering($input: OfferingInput) {
    offering: createOffering(input: $input) {
      id
      name
      logo
      description
      pageName
      type
      market {
        id
        name
      }      
      isCommercial
      commercialType
    }
  }
`;



module.exports.delete = /* GraphQL */ `
  mutation DeleteOffering($offeringId: ID!) {
    deleteOfferingStatus: deleteOffering(offeringId: $offeringId)
  }
`;

module.exports.valueProposition = {
  getTypes: /* GraphQL */ `
    query GetOfferingValuePropositionTypes(
      $category: ValuePropositionCategory!
    ) {
      valuePropositionTypes: getOfferingValuePropositionTypes(
        category: $category
      )
    }
  `,

  add: /* GraphQL */ `
    mutation AddOfferingValueProposition(
      $offeringId: ID!
      $category: ValuePropositionCategory!
      $input: ValuePropositionItemInput!
    ) {
      valueProposition: addOfferingValueProposition(
        offeringId: $offeringId
        category: $category
        input: $input
      ) {
        id
        type
        description
      }
    }
  `,

  delete: /* GraphQL */ `
    mutation DeleteOfferingValueProposition(
      $offeringId: ID!
      $category: ValuePropositionCategory!
      $valuePropositionId: ID!
    ) {
      deleteValuePropositionStatus: deleteOfferingValueProposition(
        offeringId: $offeringId
        category: $category
        valuePropositionId: $valuePropositionId
      )
    }
  `
};

module.exports.prototype = {
  listPlatforms: /* GraphQL */ `
    query ListOfferingPrototypePlatforms($offeringId: ID!, $isActive: Boolean) {
      platforms: listOfferingPrototypePlatforms(
        offeringId: $offeringId
        isActive: $isActive
      ) {
        id
        name
        description
        icon
        configuration {
          name
          description
          value
        }
      }
    }
  `,

  getPlatform: /* GraphQL */ `
    query GetPrototypePlatform($offeringId: ID!, $platformId: ID!) {
      platform: getPrototypePlatform(offeringId: $offeringId, platformId: $platformId) {
        id
        name
        description
        icon
        configuration {
          name
          description
          value
        }
        form {
          component
          attributes {
            required
            className
            title
            name
          }
          content
        }
      }
    }
  `,

  setup: /* GraphQL */ `
    mutation SetupPrototypePlatform(
      $offeringId: ID!
      $platformId: ID!
      $configuration: [ParameterInput]
    ) {
      platform: setupPrototypePlatform(
        offeringId: $offeringId
        platformId: $platformId
        configuration: $configuration
      ) {
        id
        name
        description
        icon
        configuration {
          name
          description
          value
        }
        form {
          component
          attributes {
            required
            className
            title
            name
          }
          content
        }
      }
    }
  `
};