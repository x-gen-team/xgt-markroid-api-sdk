const AppSyncService = require('../../core/appsync-service');
const interactions = require('../../interactions/business-metadata');

class LegalEntityService extends AppSyncService {
  
  getConfiguration() {
    return  require('../../configs/legacy');
  }
  
  async list() {
    return await this.invoke(
      interactions.legalEntity.list
    );
  };

  async getById({legalEntityId}) {
    return await this.invoke(
      interactions.legalEntity.get,
      { id: legalEntityId }
    );
  };

}


module.exports = LegalEntityService;