module.exports.setup = /* GraphQL */ `
  mutation SetupOfferingStrategy($offeringId: ID!, $strategyId: ID!) {
    strategy: setupOfferingStrategy(offeringId: $offeringId, strategyId: $strategyId) {
      id
      type
      name
      description
      mode
      sections
      details {
        objectives {
          id
          name
          isEnabled
          criteria {
            id
            name
            importance
            settings {
              property
              condition
              value
            }
          }
          actions {
            id
            name
            description
            isAutomated
            assignee {
              accountId
              name
              ownership
            }
            due
            checkpoints
          }
          resources
          integrations
        }
        settings {
          team {
            accountId
            name
            role {
              id
              name
              description
            }
            ownership
          }
          trackingIntegrations {
            id
            type
            name
            icon
          }
          subscriptions {
            id
            type
            receiver
          }
        }
      }
    }
  }
`;

module.exports.saveConfiguration = /* GraphQL */ `
  mutation SaveOfferingStrategyConfiguration(
    $offeringId: ID!
    $configuration: StrategyDetailsInput!
  ) {
    strategy: saveOfferingStrategyConfiguration(
      offeringId: $offeringId
      configuration: $configuration
    ) {
      id
      type
      name
      description
      mode
      sections
      details {
        objectives {
          id
          name
          isEnabled
          criteria {
            id
            name
            importance
            settings {
              property
              condition
              value
            }
          }
          actions {
            id
            name
            description
            isAutomated
            assignee {
              accountId
              name
            }
            due
            checkpoints
          }
          resources
          integrations
        }
        settings {
          team {
            accountId
            name
            role {
              id
              name
              description
            }
          }
          trackingIntegrations {
            id
            type
            name
            icon
          }
          subscriptions {
            id
            type
            receiver
          }
        }
      }
    }
  }
`;
module.exports.provision = /* GraphQL */ `
  mutation ProvisionOfferingStrategy(
    $offeringId: ID!
    $configuration: StrategyDetailsInput!
  ) {
    strategy: provisionOfferingStrategy(
      offeringId: $offeringId
      configuration: $configuration
    ) {
      id
      type
      name
      description
      mode
      sections
      details {
        state
        objectives {
          id
          name
          isEnabled
          state
          criteria {
            id
            name
            importance
            state
            settings {
              property
              condition
              value
            }
          }
          actions {
            id
            name
            description
            isAutomated
            assignee {
              accountId
              name
              ownership
            }
            state
            due
            checkpoints
          }
          resources
          integrations
        }
        notifications {
          type
          message
          actionItems
        }
        settings {
          team {
            accountId
            name
            role {
              id
              name
              description
            }
          }
          trackingIntegrations {
            id
            type
            name
            icon
          }
          subscriptions {
            id
            type
            receiver
          }
        }
      }
    }
  }
`;

module.exports.onExecutionStateChanged = /* GraphQL */ `
  subscription OnOfferingStrategyExecutionStateChanged {
    onOfferingStrategyExecutionStateChanged {
      id
      type
      name
      description
      mode
      sections
      details {
        state
        objectives {
          id
          name
          isEnabled
          state
          criteria {
            id
            name
            importance
            state
            settings {
              property
              condition
              value
            }
          }
          actions {
            id
            name
            description
            isAutomated
            assignee {
              accountId
              name
              ownership
            }
            state
            due
            checkpoints
          }
          resources
          integrations
        }
        notifications {
          type
          message
          actionItems
        }
        settings {
          team {
            accountId
            name
            role {
              id
              name
              description
            }
            ownership
            contracts {
              id
              name
              description
              url
            }
          }
          trackingIntegrations {
            id
            type
            name
            icon
          }
          subscriptions {
            id
            type
            receiver
          }
        }
      }
    }
  }
`;


module.exports.list = /* GraphQL */ `
  query ListStrategies {
    strategies: listStrategies {
      id
      type
      name
      description
    }
  }
`;
module.exports.getById = /* GraphQL */ `
  query GetStrategy($id: ID!) {
    strategy: getStrategy(id: $id) {
      id
      type
      name
      description
    }
  }
`;
