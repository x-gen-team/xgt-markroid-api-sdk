module.exports.get = /* GraphQL */ `
  query GetOrganization {
    organization: getOrganization {
      id
      name
      description
      logo
      legalEntity { id, name, description  }
      industry { id, name }
      markets { id, name }
      primaryAddress { country, state, city, street, unit, postalCode }
      startingTime
      performance {
        metric
        ticker { 
          dimension, scale, value
          change { absolute, relative }
        }
        span { start, end }
        dataPoints {
          dimensions, scales, values 
          change { absolute, relative  }
        }
      }
    }
  }
`;

module.exports.getPerformance = /* GraphQL */ `
  query GetOrganizationPerformance($filter: OrganizationPerformanceFilter!) {
    organizationPerformance: getOrganizationPerformance(filter: $filter) {
      metric
      ticker {
        dimension
        scale
        value
        change {
          absolute
          relative
        }
      }
      span {
        start
        end
      }
      dataPoints {
        dimensions
        scales
        values
        change {
          absolute
          relative
        }
      }
    }
  }
`;

module.exports.save = /* GraphQL */ `
  mutation SaveOrganization($input: OrganizationInput!) {
    organization: saveOrganization(input: $input) {
      id
      name
      description
      logo
      legalEntity {
        id
        name
        description
      }
      industry {
        id
        name
      }
      markets {
        id
        name
      }
      primaryAddress {
        country
        state
        city
        street
        unit
        postalCode
      }
      startingTime
      performance {
        metric
        ticker {
          dimension
          scale
          value
          change {
            absolute
            relative
          }
        }
        span {
          start
          end
        }
        dataPoints {
          dimensions
          scales
          values
          change {
            absolute
            relative
          }
        }
      }
    }
  }
`;
module.exports.delete = /* GraphQL */ `
  mutation DeleteOrganization {
    deleteOrganizationStatus: deleteOrganization
  }
`;