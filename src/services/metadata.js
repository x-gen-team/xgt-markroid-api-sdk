const AppSyncService = require('../core/appsync-service');
const interactions = require('../interactions/metadata');

/**
 * 
 * Organization API Class provides an interface 
 * for Organization API
 * 
 */
class MetadataService extends AppSyncService {
  
  getConfiguration() {
    return  require('../configs/legacy');
  }

  /**
   * Get Organization for current account
   */
  async get({metadataId}) {
    let response = await this.invoke(
      interactions.get, 
      {
        metadataId
      }
    );
    let metadata = response.data.metadata;
    return {
      data: JSON.parse(metadata ? metadata.data : '{}')
    }
  };

}


module.exports = MetadataService;