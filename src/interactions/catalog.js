module.exports.list = /* GraphQL */ `
  query ListCatalog {
    catalog: listCatalog {
      id
      offering {
        id
        name
      }
      type
      name
      picture
      description
      availability
      cost
    }
  }
`;
module.exports.getByOfferingId = /* GraphQL */ `
  query GetCatalogByOfferingId($offeringId: ID!) {
    catalog: getCatalogByOfferingId(offeringId: $offeringId) {
      id
      offering {
        id
        name
      }
      type
      name
      picture
      description
      availability
      cost
    }
  }
`;
module.exports.getItemById = /* GraphQL */ `
  query GetCatalogItemById($itemId: ID!) {
    item: getCatalogItemById(itemId: $itemId) {
      id
      offering {
        id
        name
      }
      type
      name
      picture
      description
      availability
      cost
    }
  }
`;

module.exports.addItem = /* GraphQL */ `
  mutation AddCatalogItem($offeringId: ID!, $input: CatalogItemInput!) {
    catalogItem: addCatalogItem(offeringId: $offeringId, input: $input) {
      id
      offering {
        id
        name
      }
      type
      name
      picture
      description
      availability
      cost
    }
  }
`;
module.exports.updateItem = /* GraphQL */ `
  mutation UpdateCatalogItem($itemId: ID!, $input: CatalogItemInput!) {
    catalogItem: updateCatalogItem(itemId: $itemId, input: $input) {
      id
      offering {
        id
        name
      }
      type
      name
      picture
      description
      availability
      cost
    }
  }
`;
module.exports.deleteItem = /* GraphQL */ `
  mutation DeleteCatalogItem($itemId: ID!) {
    deleteCatalogItemStatus: deleteCatalogItem(itemId: $itemId)
  }
`;