const interactions          = require('../interactions/catalog');
const AppSyncService = require('../core/appsync-service');

/**
 * 
 * Offering Service provides an interface 
 * for Offering API
 * 
 */
class CatalogService extends AppSyncService{

  getConfiguration() {
    return  require('../configs/legacy');
  }

  async list() {
    return await this.invoke(interactions.list);
  }

  async getItemById({itemId}) {
    return await this.invoke(
      interactions.getItemById,
      { itemId }
    );
  }

  async getByOfferingId({offeringId}) {
    return await this.invoke(
      interactions.getByOfferingId,
      { offeringId }
    );
  }

  async addItem ({offeringId, item}) {
    return await this.invoke(
      interactions.addItem,
      { 
        offeringId, 
        input: item
      }
    );

  }

  async deleteItem({itemId}) {
    return await this.invoke(
      interactions.deleteItem,
      { 
        itemId
      }
    );

  }

  async updateItem({itemId, item}) {
    return await this.invoke(
      interactions.updateItem,
      { 
        itemId,
        input: item
      }
    );
  }


};

module.exports = CatalogService;