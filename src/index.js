const AmplifyCore = require('aws-amplify')
const Amplify = require('aws-amplify').default

const { API } = Amplify
const { graphqlOperation } = AmplifyCore

const OrganizationService = require('./services/organization');
const SystemPricingTagService = require('./services/system-pricing-tag');
const IndustryService = require('./services/business-metadata/industry');
const LegalEntityService = require('./services/business-metadata/legal-entity');
const UserAccountService = require('./services/user-account');
const OfferingService = require('./services/offering');
const OfferingValuePropositionService = require('./services/offering/value-proposition');
const OfferingPrototypeService = require('./services/offering/prototype');
const StrategyService = require('./services/strategy');
const CatalogService = require('./services/catalog');
//const Accounting = require('./services/accounting');

module.exports = {

  OrganizationService,
  SystemPricingTagService,
  IndustryService,
  LegalEntityService,
  UserAccountService,
  OfferingService,
  OfferingValuePropositionService,
  OfferingPrototypeService,
  StrategyService,
  CatalogService
  /*BusinessMetadata: {
    Industry,
    LegalEntity
  },
  Offering,
  Strategy,
  Catalog,
  Accounting*/
};