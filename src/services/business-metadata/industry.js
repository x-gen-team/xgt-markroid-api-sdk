const AppSyncService = require('../../core/appsync-service');
const interactions = require('../../interactions/business-metadata');

class IndustryService extends AppSyncService {

  getConfiguration() {
    return require('../../configs/legacy');
  }

  async list() {
    return await this.invoke(
      interactions.industry.list
    );
  };


  async getById({industryId}) {
    return await this.invoke(
      interactions.industry.get,
      { id: industryId }
    );
  
  };


}

module.exports = IndustryService;