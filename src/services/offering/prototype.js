const interactions          = require('../../interactions/offering');
const AppSyncService = require('../../core/appsync-service');

class OfferingPrototypeService extends AppSyncService{

  getConfiguration() {
    return  require('../../configs/legacy');
  }


  async listPlatforms ({offeringId, isActive}) {
    return await this.invoke(
      interactions.prototype.listPlatforms, 
      {offeringId, isActive}
    );
  };

  async getPlatform ({offeringId, platformId}) {
    let response = await this.invoke(
      interactions.prototype.getPlatform, 
      {offeringId, platformId}
    );
    if(response && response.data && response.data.platform.form){
      for(let i = 0; i < response.data.platform.form.length; i++){
        response.data.platform.form[i].content = JSON.parse(response.data.platform.form[i].content);
      }
      
    }
    return response;
  };

  async setup ({offeringId, platformId, configuration}) {
    let response = await this.invoke(
      interactions.prototype.setup, 
      {offeringId, platformId, configuration}
    );
    if(response && response.data && response.data.platform.form){
      for(let i = 0; i < response.data.platform.form.length; i++){
        response.data.platform.form[i].content = JSON.parse(response.data.platform.form[i].content);
      }
      
    }
    return response;
  };
  
  

};


module.exports = OfferingPrototypeService;