const Service = require('./service');
let AmplifyCore = require('aws-amplify');
let Amplify = require('aws-amplify').default;
let { graphqlOperation } = AmplifyCore;


class AppSyncService extends Service {

  getConfiguration() {
    return {};
  }

  initialize() {
    Amplify.configure(this.getConfiguration());
  }
  
  async invoke(operation, data={}) {
    this.initialize();
    let API = Amplify.API;
    return await API.graphql( 
      graphqlOperation( operation, data ) 
    );

  }

}

module.exports = AppSyncService;