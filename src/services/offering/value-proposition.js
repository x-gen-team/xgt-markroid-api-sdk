const interactions          = require('../../interactions/offering');
const AppSyncService = require('../../core/appsync-service');

class OfferingValuePropositionService extends AppSyncService{
  
  getConfiguration() {
    return  require('../../configs/legacy');
  }

  async getTypes({category}) {
    return await this.invoke(
      interactions.valueProposition.getTypes, 
      { category } 
    );

  }

  async add({offeringId, category, item}) {
    return await this.invoke(
      interactions.valueProposition.add,  
      { 
        offeringId, 
        category, 
        input: item 
      }
    );
  }

  async getFeatureTypes() {
    let response = await this.getTypes({category: OfferingValuePropositionService.categories.FEATURE});
    return {
      data: {
        featureTypes: response.data.valuePropositionTypes
      }
    };
  }
  
  async addFeature ({offeringId, feature}) {

    return await this.add({
      offeringId,
      category: OfferingValuePropositionService.categories.FEATURE,
      item: feature
    });
  };
  
  async getBenefitTypes() {
    let response = await this.getTypes({category: OfferingValuePropositionService.categories.BENEFIT});
    return {
      data: {
        benefitTypes: response.data.valuePropositionTypes
      }
    };
  };
  
  async addBenefit ({offeringId, benefit}) {
    return await this.add({
      offeringId,
      category: OfferingValuePropositionService.categories.BENEFIT,
      item: benefit
    });
  };

  async getCompetitiveAdvantageTypes() {
    let response = await this.getTypes({category: OfferingValuePropositionService.categories.COMPETITIVE_ADVANTAGE});
    return {
      data: {
        competitiveAdvantageTypes: response.data.valuePropositionTypes
      }
    };
  };
  
  async addCompetitiveAdvantage ({offeringId, competitiveAdvantage}) {

    return await this.add({
      offeringId,
      category: OfferingValuePropositionService.categories.COMPETITIVE_ADVANTAGE,
      item: competitiveAdvantage
    });
  };

  async deleteFeature ({offeringId, featureId}) { 
    return await this.delete({
      offeringId,
      category: OfferingValuePropositionService.categories.FEATURE,
      valuePropositionId: featureId
    });
  }
  
  async deleteBenefit ({offeringId, benefitId}) {
    return await this.delete({
      offeringId,
      category: OfferingValuePropositionService.categories.BENEFIT,
      valuePropositionId: benefitId
    });
  };
  
  async deleteCompetitiveAdvantage ({offeringId, competitiveAdvantageId}) {

    return await this.delete({
      offeringId,
      category: OfferingValuePropositionService.categories.COMPETITIVE_ADVANTAGE,
      valuePropositionId: competitiveAdvantageId
    });
  }

  async delete ({offeringId, category, valuePropositionId}) {
    return await this.invoke(
      interactions.OfferingValuePropositionService.delete, 
      { 
        offeringId, 
        category, 
        valuePropositionId
      } 
    );
  }

};

OfferingValuePropositionService.categories = {
  FEATURE: 'FEATURE',
  BENEFIT: 'BENEFIT',
  COMPETITIVE_ADVANTAGE: 'COMPETITIVE_ADVANTAGE'
};

module.exports = OfferingValuePropositionService;