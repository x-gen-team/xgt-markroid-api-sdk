const interactions = require('../interactions/user-account');
const AppSyncService = require('../core/appsync-service');

/**
 * 
 * UserAccount Service provides an interface 
 * for UserAccount API
 * 
 */
class UserAccountService extends AppSyncService {

  getConfiguration() {
    return  require('../configs/user-account');
  }

  /**
   * Get User Account Information
   */
  async getInfo() {
    return await this.invoke(interactions.getInfo);
  };

  /**
   * Register User Account
   */
  async create() {
    return await this.invoke(interactions.create);
  };
};


module.exports = UserAccountService;