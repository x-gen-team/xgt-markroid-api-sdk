const interactions = require('../interactions/organization');
const AppSyncService = require('../core/appsync-service');

/**
 * 
 * Organization Service provides an interface 
 * for Organization API
 * 
 */

class OrganizationService extends AppSyncService{

  getConfiguration() {
    return  require('../configs/legacy');
  }

  /**
   * Get Organization for current account
   */
  async get() {
    return await this.invoke(interactions.get);
  };

  /**
   * Get Organization Performance
   * @param string metric 
   * @param string timeframe 
   */
  async getPerformance({metric, timeframe}) {
    return await this.invoke(
      interactions.getPerformance,
      {
        filter: {
          metric,
          timeframe
        }
      }
    );
  };

  /**
   * Save organization for current account
   * @param {*} data 
   */
  async save({input}) {
    return await this.invoke(
      interactions.save,
      { input }
    );
    
  };

  /**
   * Delete current account's organization
   */
  async delete() {
    return await this.invoke(
      interactions.delete
    );
  };
};

module.exports = OrganizationService;