module.exports.list = /* GraphQL */ `
  query List {
    pricingTags: listPricingTags {
      icon
      id
      name
      price {
        amount
        startsFrom
        units
      }
  }
}`;

