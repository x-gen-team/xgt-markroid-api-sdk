const interactions          = require('../interactions/strategy');
const AppSyncService = require('../core/appsync-service');


/**
 * 
 * Offering Service provides an interface 
 * for Offering API
 * 
 */
class StrategyService extends AppSyncService {
  constructor() {
    super();
    this.executionStateChangedListener = {};
  }

  getConfiguration() {
    return  require('../configs/legacy');
  }

  async list () {
    return await this.invoke(interactions.list);
  }

  async getById({strategyId}) {
    return await this.invoke(
      interactions.getById,
      { id: strategyId }
    );

  };

  async setup ({offeringId, strategyId}) {
    return await this.invoke(
      interactions.setup,
      { 
        offeringId, 
        strategyId
      }
    );
  }

  async saveConfiguration({offeringId, configuration}) {
    return await this.invoke(
      interactions.saveConfiguration,
      { 
        offeringId, 
        configuration
      } 
    );

  };

  async provision ({offeringId, configuration}) {
    return await this.invoke(
      interactions.provision,
      { 
        offeringId, 
        configuration
      } 
    );

  }

  /*onExecutionStateChanged = {
    subscribe: async ({offeringId, callback}) => {

      if(this.executionStateChangedListener[offeringId]){
        throw new Error('Listener already subscribed for this instance');
      }
      this.executionStateChangedListener = await API.graphql(
        graphqlOperation(
          interactions.onExecutionStateChanged,
          { offeringId }
        )
      ).subscribe({
        next: (strategy) => {
          callback(null, strategy);
        },
        error: error => {
          console.warn(error);
          callback(error, null);
        }
      });
    },

    unsubscribe: ({offeringId}) => {

      if(this.executionStateChangedListener[offeringId]){
        this.executionStateChangedListener[offeringId].unsubscribe();
      }
    }
  };
  
  this.onExecutionComplete = {
    subscribe: async ({offeringId, callback}) => {

      if(this.executionStateChangedListener[offeringId]){
        throw new Error('Listener already subscribed for this instance');
      }
      this.executionStateChangedListener = await API.graphql(
        graphqlOperation(
          interactions.onExecutionStateChanged,
          { offeringId }
        )
      ).subscribe({
        next: (strategy) => {
          callback(null, strategy);
        },
        error: error => {
          console.warn(error);
          callback(error, null);
        }
      });
    },

    unsubscribe: ({offeringId}) => {

      if(this.executionStateChangedListener[offeringId]){
        this.executionStateChangedListener[offeringId].unsubscribe();
      }
    }
  };

  this.onDataCollectionComplete = async () => {
    
  };*/
}



module.exports = StrategyService;